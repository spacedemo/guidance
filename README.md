{
  "identifier": "TEST",
  "orbitDescriptor": {
    "a": 7200000,
    "e": 0.01,
    "i": 98,
    "pa": 0,
    "raan": 0,
    "anomaly": 0,
    "date": "2010-01-01T12:00:00Z"
  }
}