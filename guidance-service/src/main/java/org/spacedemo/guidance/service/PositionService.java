package org.spacedemo.guidance.service;

import java.time.Instant;

import org.spacedemo.guidance.controller.PositionController;
import org.spacedemo.guidance.model.Interval;
import org.spacedemo.guidance.model.Position;
import org.spacedemo.guidance.model.PositionDates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class PositionService {

	@Autowired
	PositionController positionController;

	@RequestMapping(value = "/position", method = RequestMethod.GET)
	public PositionDates trace(@RequestParam Instant begin, @RequestParam Instant end, @RequestParam int step_ms) {
		return positionController.getTrace(new Interval(begin, end), step_ms);
	}

	@RequestMapping(value = "/position/{date}", method = RequestMethod.GET)
	public Position position(@PathVariable("date") Instant date) {
		return positionController.getPosition(date);
	}

}
