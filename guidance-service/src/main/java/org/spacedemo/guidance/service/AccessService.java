package org.spacedemo.guidance.service;

import java.util.Arrays;

import org.spacedemo.guidance.controller.AccessController;
import org.spacedemo.guidance.model.Accesses;
import org.spacedemo.guidance.model.AccessParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class AccessService {

	@Autowired
	AccessController accessController;

	@RequestMapping(value = "/acceses", method = RequestMethod.POST)
	public Accesses acceses(@RequestParam AccessParameters parameters) {
		return accessController.getAccesses(Arrays.asList(parameters.getPosition()), parameters.getInterval());
	}

}
