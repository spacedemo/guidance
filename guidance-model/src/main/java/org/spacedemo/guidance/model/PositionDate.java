package org.spacedemo.guidance.model;

import java.time.Instant;

public class PositionDate {

	private Instant date;
	private Position position;

	public PositionDate(Instant date, Position position) {
		this.date = date;
		this.position = position;
	}

	public Instant getDate() {
		return date;
	}

	public void setDate(Instant date) {
		this.date = date;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

}
