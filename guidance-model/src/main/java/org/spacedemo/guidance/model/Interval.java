package org.spacedemo.guidance.model;

import java.time.Instant;

public class Interval {

	private Instant begin;
	private Instant end;

	public Interval(Instant begin, Instant end) {
		this.begin = begin;
		this.end = end;
	}

	public Instant getBegin() {
		return begin;
	}

	public void setBegin(Instant begin) {
		this.begin = begin;
	}

	public Instant getEnd() {
		return end;
	}

	public void setEnd(Instant end) {
		this.end = end;
	}

}
