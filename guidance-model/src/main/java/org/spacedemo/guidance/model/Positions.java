package org.spacedemo.guidance.model;

import java.util.List;

public class Positions {

	private List<Position> positions;

	public List<Position> getPositions() {
		return positions;
	}

	public void setPositions(List<Position> positions) {
		this.positions = positions;
	}

}
