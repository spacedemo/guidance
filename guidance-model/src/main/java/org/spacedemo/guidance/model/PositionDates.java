package org.spacedemo.guidance.model;

import java.util.List;

public class PositionDates {

	private List<PositionDate> positionDates;

	public List<PositionDate> getPositionDates() {
		return positionDates;
	}

	public void setPositionDates(List<PositionDate> positionDates) {
		this.positionDates = positionDates;
	}


}
