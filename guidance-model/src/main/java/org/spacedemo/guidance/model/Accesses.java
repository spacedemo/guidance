package org.spacedemo.guidance.model;

import java.util.List;

public class Accesses {

	private List<Access> accesses;

	public List<Access> getAccesses() {
		return accesses;
	}

	public void setAccesses(List<Access> accesses) {
		this.accesses = accesses;
	}

}
