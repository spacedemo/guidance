FROM openjdk:8-jre-slim

COPY guidance-app/target/guidance-app.jar .

CMD ["java", "-jar", "guidance-app.jar"]
