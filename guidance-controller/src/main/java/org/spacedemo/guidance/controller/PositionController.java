package org.spacedemo.guidance.controller;

import java.time.Instant;

import org.spacedemo.guidance.controller.service.PatriusService;
import org.spacedemo.guidance.model.Interval;
import org.spacedemo.guidance.model.Position;
import org.spacedemo.guidance.model.PositionDates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class PositionController {
	
	@Autowired
	private PatriusService patrius;

	public Position getPosition(Instant date) {
		return patrius.getPosition(date);
	}
	
	/**
	 * Compute the trace of the satellite on the given period with the required
	 * step.
	 * 
	 * @param begin   trace start date in UTC
	 * @param end     trace end date in UTC
	 * @param step_ms step of the trace points in milliseconds
	 * @return the trace as a list of dated positions
	 */
	public PositionDates getTrace(Interval interval, int step_ms) {
		return patrius.getTrace(interval, step_ms);
	}
}
