package org.spacedemo.guidance.controller;

import java.util.List;

import org.spacedemo.guidance.controller.service.PatriusService;
import org.spacedemo.guidance.model.Accesses;
import org.spacedemo.guidance.model.Interval;
import org.spacedemo.guidance.model.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class AccessController {
	
	@Autowired
	private PatriusService patrius;

	/**
	 * Compute the accesses to the given positions
	 * 
	 * @param positions the positions of the user requests
	 * @param interval  of dates to compute the accesses on
	 * @return the accesses
	 */
	public Accesses getAccesses(List<Position> positions, Interval interval) {
		return patrius.getAccesses(positions, interval);
	}
}
