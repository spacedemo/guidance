package org.spacedemo.guidance.controller.service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.spacedemo.guidance.model.Access;
import org.spacedemo.guidance.model.Accesses;
import org.spacedemo.guidance.model.Interval;
import org.spacedemo.guidance.model.Position;
import org.spacedemo.guidance.model.PositionDate;
import org.spacedemo.guidance.model.PositionDates;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import fr.cnes.sirius.addons.patriusdataset.PatriusDataset;
import fr.cnes.sirius.patrius.bodies.ExtendedOneAxisEllipsoid;
import fr.cnes.sirius.patrius.bodies.GeodeticPoint;
import fr.cnes.sirius.patrius.events.EarthZoneDetector;
import fr.cnes.sirius.patrius.frames.Frame;
import fr.cnes.sirius.patrius.frames.FramesFactory;
import fr.cnes.sirius.patrius.frames.configuration.FramesConfiguration;
import fr.cnes.sirius.patrius.frames.configuration.FramesConfigurationBuilder;
import fr.cnes.sirius.patrius.math.util.FastMath;
import fr.cnes.sirius.patrius.orbits.KeplerianOrbit;
import fr.cnes.sirius.patrius.orbits.PositionAngle;
import fr.cnes.sirius.patrius.orbits.orbitalparameters.KeplerianParameters;
import fr.cnes.sirius.patrius.propagation.Propagator;
import fr.cnes.sirius.patrius.propagation.SpacecraftState;
import fr.cnes.sirius.patrius.propagation.analytical.KeplerianPropagator;
import fr.cnes.sirius.patrius.propagation.events.EventDetector;
import fr.cnes.sirius.patrius.propagation.events.EventDetector.Action;
import fr.cnes.sirius.patrius.propagation.sampling.PatriusFixedStepHandler;
import fr.cnes.sirius.patrius.time.AbsoluteDate;
import fr.cnes.sirius.patrius.time.TimeScale;
import fr.cnes.sirius.patrius.time.TimeScalesFactory;
import fr.cnes.sirius.patrius.utils.Constants;
import fr.cnes.sirius.patrius.utils.exception.PatriusException;
import fr.cnes.sirius.patrius.utils.exception.PropagationException;

@Service
public class PatriusService {

	private static Frame ITRF;
	private static Frame GCRF;
	private static ExtendedOneAxisEllipsoid EARTH;
	private static TimeScale UTC;
	private static KeplerianOrbit ORBIT;

	// SPOT 7 inclination and semi major axis
	private double orbit_a = 7076e3;
	private double orbit_e = 0;
	private double orbit_i = 98.2;
	private double orbit_pa = 0;
	private double orbit_raan = 98.2318;
	private double orbit_anomaly = 0;
	private String orbit_date = "2020-01-28T15:00:00Z";

//	@Value("${orbit.a}")
//	private float orbit_a;
//	@Value("${orbit.e}")
//	private float orbit_e;
//	@Value("${orbit.i}")
//	private float orbit_i;
//	@Value("${orbit.pa}")
//	private float orbit_pa;
//	@Value("${orbit.raan}")
//	private float orbit_raan;
//	@Value("${orbit.anomaly}")
//	private float orbit_anomaly;
//	@Value("${orbit.date}")
//	private String orbit_date;

	/**
	 * Initialize the configuration for Patrius, mainly the resource files from
	 * PatriusDataset and the Frame configuration
	 */
	@PostConstruct
	public void initialize() {
		try {
			// Patrius resources
			PatriusDataset.addResourcesFromPatriusDataset();

			// Patrius configuration
			FramesFactory.setConfiguration(getSimplifiedConfiguration());

			// Useful static objects
			ITRF = FramesFactory.getITRF();
			GCRF = FramesFactory.getGCRF();
			EARTH = new ExtendedOneAxisEllipsoid(Constants.WGS84_EARTH_EQUATORIAL_RADIUS,
					Constants.WGS84_EARTH_FLATTENING, ITRF, "EARTH");
			UTC = TimeScalesFactory.getUTC();

			// Creation of a keplerian orbit
			final double inc = FastMath.toRadians(orbit_i);
			final double pa = FastMath.toRadians(orbit_pa);
			final double raan = FastMath.toRadians(orbit_raan);
			final double anm = FastMath.toRadians(orbit_anomaly);
			final double MU = Constants.WGS84_EARTH_MU;
			final KeplerianParameters kep = new KeplerianParameters(orbit_a, orbit_e, inc, pa, raan, anm,
					PositionAngle.MEAN, MU);
			ORBIT = new KeplerianOrbit(kep, GCRF, new AbsoluteDate(orbit_date, UTC));
		} catch (PatriusException err) {
			throw new RuntimeException(err);
		}
	}

	/**
	 * Patrius frames transformations configuration
	 * 
	 * @return configuration
	 */
	public static FramesConfiguration getSimplifiedConfiguration() {

		// Configurations builder
		final FramesConfigurationBuilder builder = new FramesConfigurationBuilder();

		// Config Electra :
		// // Tides and libration
		// final TidalCorrectionModel tides = TidalCorrectionModelFactory.NO_TIDE;
		// final LibrationCorrectionModel lib =
		// LibrationCorrectionModelFactory.NO_LIBRATION;
		//
		// // Polar Motion
		// final PolarMotion defaultPolarMotion = new PolarMotion(false, tides, lib,
		// SPrimeModelFactory.NO_SP);
		//
		// // Diurnal rotation
		// final DiurnalRotation defaultDiurnalRotation = new DiurnalRotation(tides,
		// lib);
		//
		// // Precession Nutation
		// final PrecessionNutation precNut = new PrecessionNutation(false,
		// PrecessionNutationModelFactory.PN_IERS2010_INTERPOLATED_NON_CONSTANT);
		//
		// builder.setDiurnalRotation(defaultDiurnalRotation);
		// builder.setPolarMotion(defaultPolarMotion);
		// builder.setPrecessionNutation(precNut);
		// builder.setEOPHistory(new NoEOP2000History());
		return builder.getConfiguration();
	}

	public Position getPosition(Instant date) {
		return new Position(0, 0);
	}

	/**
	 * Compute the trace of the satellite on the given period with the required
	 * step.
	 * 
	 * @param begin   trace start date in UTC
	 * @param end     trace end date in UTC
	 * @param step_ms step of the trace points in milliseconds
	 * @return the trace as a list of dated positions
	 */
	public PositionDates getTrace(Interval interval, int step_ms) {
		PositionDates trace = new PositionDates();
		trace.setPositionDates(new ArrayList<PositionDate>());

		try {
			Propagator propagator = new KeplerianPropagator(ORBIT);

			double step = step_ms / 1000.;

			// Define a step handler to store the position of the spacecraft at each step
			propagator.setMasterMode(step, new PatriusFixedStepHandler() {

				@Override
				public void init(SpacecraftState s0, AbsoluteDate date) {
					// Nothing to do here
				}

				@Override
				public void handleStep(SpacecraftState currentState, boolean isLast) throws PropagationException {
					try {
						// Store the trace position
						Instant instant = buildInstantFromAbsoluteDate(currentState.getDate());
						Position position = buildPositionFromSpacecraftState(currentState);
						trace.getPositionDates().add(new PositionDate(instant, position));
					} catch (PatriusException err) {
						throw new RuntimeException(err);
					}
				}
			});

			// Propagate the orbit
			propagator.propagate(buildInstantFromAbsoluteDate(interval.getBegin()),
					buildInstantFromAbsoluteDate(interval.getEnd()));

		} catch (PatriusException patriusException) {
			throw new RuntimeException(patriusException);
		}

		return trace;
	}

	/**
	 * Transform an AbsoluteDate into an Instant
	 * 
	 * @param date
	 * @return the corresponding Instant in UTC
	 */
	protected Instant buildInstantFromAbsoluteDate(AbsoluteDate date) {
		Instant instant = null;
		try {
			instant = Instant.parse(date.toString(TimeScalesFactory.getUTC()) + "Z");
		} catch (PatriusException err) {
			throw new RuntimeException(err);
		}
		return instant;
	}

	/**
	 * Transform an Instant into an AbsoluteDate
	 * 
	 * @param instant
	 * @return the corresponding AbsoluteDate
	 */
	protected AbsoluteDate buildInstantFromAbsoluteDate(Instant instant) {
		return new AbsoluteDate(instant.toString(), UTC);
	}

	/**
	 * Build the trace position under the in-orbit PV coordinates
	 * 
	 * @param state
	 * @return the Geodetic point on earth under the orbit
	 * @throws PatriusException
	 */
	protected Position buildPositionFromSpacecraftState(SpacecraftState state) throws PatriusException {

		GeodeticPoint point = EARTH.transform(state.getPVCoordinates().getPosition(), state.getFrame(),
				state.getDate());

		return new Position(point.getLongitude(), point.getLatitude());
	}

	/**
	 * Compute the accesses to the given positions
	 * 
	 * @param positions the positions of the user requests
	 * @param interval  of dates to compute the accesses on
	 * @return the accesses
	 */
	public Accesses getAccesses(List<Position> positions, Interval interval) {

		Accesses accesses = new Accesses();
		accesses.setAccesses(new ArrayList<Access>());

		try {
			Propagator propagator = new KeplerianPropagator(ORBIT);

			// Define a Detector for each of the positions
			for (Position position : positions) {
				propagator.addEventDetector(buildDetectorForPosition(position, accesses));
			}

			// Propagate the orbit
			propagator.propagate(buildInstantFromAbsoluteDate(interval.getBegin()),
					buildInstantFromAbsoluteDate(interval.getEnd()));

		} catch (PropagationException err) {
			throw new RuntimeException(err);
		}
		return accesses;
	}

	/**
	 * Build a detector used in the propagation to find the accesses to the given on
	 * ground position
	 * 
	 * @param position the ground position
	 * @param accesses this list is filled by the detector as the accesses are found
	 * @return the tailored event detector
	 */
	protected EventDetector buildDetectorForPosition(Position position, Accesses accesses) {
		List<double[][]> zone = new ArrayList<double[][]>();
		double lat = position.getLatitude();
		double lon = position.getLongitude();

		// TODO a relier aux paramètres ALONG_TRACK et ACROSS_TRACK et prendre en compte
		// des marges
		double rangeLat = FastMath.toRadians(10);
		double rangeLon = FastMath.toRadians(10);

		zone.add(new double[][] { { lat - rangeLat, lon + rangeLon }, { lat + rangeLat, lon + rangeLon },
				{ lat + rangeLat, lon - rangeLon }, { lat - rangeLat, lon - rangeLon } });

		return new EarthZoneDetector(EARTH, zone, EarthZoneDetector.DEFAULT_MAXCHECK,
				EarthZoneDetector.DEFAULT_THRESHOLD, Action.CONTINUE, Action.CONTINUE, false, false) {
			AbsoluteDate startDate = null;

			@Override
			public Action eventOccurred(final SpacecraftState state, final boolean increasing, final boolean forward)
					throws PatriusException {
				// Store the access start on the rising event, then the end on the decreasing
				// event
				if (increasing) {
					startDate = state.getDate();
				} else {
					// Ignore the access if the orbit start during and access => there wont be enough time to plan anyway.
					if (startDate != null) {
						accesses.getAccesses()
								.add(new Access(position, new Interval(buildInstantFromAbsoluteDate(startDate),
										buildInstantFromAbsoluteDate(state.getDate()))));
					}
				}
				return Action.CONTINUE;
			}
		};
	}

}
