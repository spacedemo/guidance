package org.spacedemo.guidance.controller.service;

import java.time.Instant;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;
import org.spacedemo.guidance.model.Access;
import org.spacedemo.guidance.model.Accesses;
import org.spacedemo.guidance.model.Interval;
import org.spacedemo.guidance.model.Position;
import org.spacedemo.guidance.model.PositionDate;
import org.spacedemo.guidance.model.PositionDates;

import org.spacedemo.guidance.controller.service.PatriusService;

import fr.cnes.sirius.patrius.utils.exception.PatriusException;

public class PatriusServiceTest {

	private static final long SEC_PER_DAY = 86400;

	public PatriusServiceTest() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Compute the trace of the satellite and print it.
	 * 
	 * The computation is run 10 times to get a mean computation time.
	 * @throws PatriusException
	 */
	@Test
	public void traceTest() throws PatriusException {		
		// Start Patrius
		PatriusService service = new PatriusService();
		service.initialize();

		// Define the trace interval
		Instant start = Instant.now();
		Instant end = start.plusSeconds(SEC_PER_DAY);
		Interval interval = new Interval(start, end);

		PositionDates trace = null;
		int nbIterations = 10;
		int step = 1000000;
		long tic = System.currentTimeMillis();
		for (int i = 0; i < nbIterations; i++) {
			// Compute the trace
			trace = service.getTrace(interval, step);
		}
		long toc = System.currentTimeMillis();

		// Check the service performance
		long meanTime = (toc - tic) / nbIterations;
		Assert.assertTrue("Mean time should not exceed 1s for 1 day trace computation", meanTime< 1000);

		long expectedNbPoints = SEC_PER_DAY/(step/1000);
		Assert.assertEquals("There should about " + expectedNbPoints + " points", expectedNbPoints, trace.getPositionDates().size(), 2);
		for (PositionDate position : trace.getPositionDates()) {
			System.out.println(position.getDate() + " " 
					+ position.getPosition().getLongitude() + " "
					+ position.getPosition().getLatitude());
		}
	}
	
	/**
	 * Compute the trace of the satellite and print it.
	 * 
	 * The computation is run 10 times to get a mean computation time.
	 * @throws PatriusException
	 */
	@Test
	public void accessTest() throws PatriusException {
		// Start Patrius
		PatriusService service = new PatriusService();
		service.initialize();

		// Define the trace interval
		Instant start = Instant.now();
		Instant end = start.plusSeconds(7 * SEC_PER_DAY);
		Interval interval = new Interval(start, end);

		Accesses accesses = null;
		int nbIterations = 10;
		long tic = System.currentTimeMillis();
		for (int i = 0; i < nbIterations; i++) {
			// Compute the accesses
			accesses = service.getAccesses(Arrays.asList(new Position(0, 0)), interval);
		}
		long toc = System.currentTimeMillis();
		
		// Check the service performance
		long meanTime = (toc - tic) / nbIterations;
		Assert.assertTrue("Mean time should not exceed 100ms for 7 day access computation", meanTime < 100);

		Assert.assertFalse("There should be some accesses", accesses.getAccesses().isEmpty());
		
		for (Access access : accesses.getAccesses()) {
			System.out.println(access.getInterval().getBegin() + " " + access.getInterval().getEnd());
		}
	}

}
